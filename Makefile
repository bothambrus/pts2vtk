init:
	pip3 install -r requirements.txt

checkstyle:
	black --check pts2vtk  
	black --check tests
	black --check configure 
	black --check convertPts2VTK 
	isort --profile black --diff pts2vtk
	isort --profile black --diff tests
	isort --profile black --diff configure
	isort --profile black --diff convertPts2VTK

style:
	isort --profile black pts2vtk
	black pts2vtk 
	black tests
	black configure
	black convertPts2VTK


test:
	rm -rf tests/tmp_testing/
	pytest --junitxml=junit_report.xml --cov=pts2vtk --cov-report=html tests
	coverage xml
	mkdir -p dist 
	rm -rf dist/htmlcov
	mv coverage.xml dist/
	mv htmlcov dist/
	echo open dist/htmlcov/index.html to review code coverage
	python .cicd_scripts/test_coverage.py 90 dist/coverage.xml



.PHONY: init test

