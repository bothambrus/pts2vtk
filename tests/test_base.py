#################################################################################
#   pts2vtk                                                                     #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import shutil
import numpy as np
import xml.etree.ElementTree as ET

from .context import pts2vtk, getTestTmpPath, getTestPath
from pts2vtk.base import BaseVTK

test_path = getTestPath()
tmp_test_path = getTestTmpPath()


def test_create_base():
    """
    Test initialization of base VTK object.
    """
    base = BaseVTK()
    assert len(base.times) == 0
    assert len(base.fileNames) == 0


def test_writing_PVD_file():
    """
    Test writing a PVD file from this class.
    """
    #
    # Create base
    #
    base = BaseVTK()

    #
    # Cilean and create directory
    #
    case_path = os.path.join(tmp_test_path, "base_writing_PVD_file")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    #
    # Add some dummy files and times
    #
    base.fileNames.append(os.path.join(case_path, "dummy1.vtu"))
    base.fileNames.append(os.path.join(case_path, "dummy2.vtu"))
    base.fileNames.append(os.path.join(case_path, "dummy3.vtu"))

    base.times.append(0.0)
    base.times.append(0.3)
    base.times.append(0.7)

    #
    # Write PVD
    #
    fn = os.path.join(case_path, "test.pvd")
    base.writePVD(fn)

    #
    # Read and check resulting file
    #
    tree = ET.parse(fn)
    root = tree.getroot()

    assert root.attrib["type"] == "Collection"
    assert root.attrib["version"] == "0.1"
    assert root.attrib["byte_order"] == "LittleEndian"
    assert root[0][0].attrib["file"] == "dummy1.vtu"
    assert root[0][0].attrib["group"] == ""
    assert root[0][0].attrib["part"] == "0"
    assert root[0][0].attrib["timestep"] == "0.0"
    assert root[0][2].attrib["file"] == "dummy3.vtu"
    assert root[0][2].attrib["group"] == ""
    assert root[0][2].attrib["part"] == "0"
    assert root[0][2].attrib["timestep"] == "0.7"


def test_reading_PVD_file():
    """
    Test reading a PVD file with this class.
    """
    #
    # Create base
    #
    base = BaseVTK()

    #
    # Cilean and create directory
    #
    case_path = os.path.join(test_path, "data", "case_example.pvd")

    #
    # Read PVD
    #
    base.readPVD(case_path)

    assert len(base.times) == 21
    assert len(base.fileNames) == 21
    assert np.abs(base.times[5] - 0.0654561729) < 1e-8
    assert base.fileNames[5] == "cam_0005.vtu"
