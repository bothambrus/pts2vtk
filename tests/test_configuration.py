#################################################################################
#   pts2vtk                                                                     #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import json
import os
import shutil

from .context import pts2vtk, getTestPath, getTestTmpPath
from pts2vtk.configuration import ProcessingConfig

tmp_test_path = getTestTmpPath()


def test_create_processing_config():
    """
    Test initialization of Processing Configuration object.
    """
    pc = ProcessingConfig()
    assert os.path.basename(pc.json_path) == "pts2vtkInputFile.json"
    assert len(pc.data.keys()) == 0


def test_reading_processing_config():
    """
    Test reading of Processing Configuration object.
    """

    case_path = os.path.join(tmp_test_path, "configuration_read")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    with open(os.path.join(case_path, "pts2vtkInputFile.json"), "w") as f:
        data = dict(CaseName="a", CasePath=case_path)
        json.dump(data, f)

    pc = ProcessingConfig(json_path=os.path.join(case_path, "pts2vtkInputFile.json"))
    assert len(pc.data.keys()) == 2
    assert pc.data["CaseName"] == "a"


def test_writing_processing_config():
    """
    Test writing of Processing Configuration object.
    """

    case_path = os.path.join(tmp_test_path, "configuration_write")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    pc = ProcessingConfig(json_path=os.path.join(case_path, "pts2vtkInputFile.json"))
    pc.data = dict(CaseName="b", CasePath=case_path)

    assert not os.path.isfile(os.path.join(case_path, "pts2vtkInputFile.json"))

    pc.writeConfig()

    with open(os.path.join(case_path, "pts2vtkInputFile.json"), "r") as f:
        data = json.load(f)
    assert len(data.keys()) == 2
    assert data["CaseName"] == "b"
