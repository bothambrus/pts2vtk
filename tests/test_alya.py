#################################################################################
#   pts2vtk                                                                     #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import shutil
import numpy as np
import xml.etree.ElementTree as ET

from .context import pts2vtk, getTestPath, getTestTmpPath
from pts2vtk.alya import AlyaParticles
from pts2vtk.base import BaseVTK

test_path = getTestPath()
tmp_test_path = getTestTmpPath()


def test_create_alya_particles():
    """
    Test initialization of Alya particles object.
    """
    ap = AlyaParticles()
    assert ap.casePath is None
    assert ap.caseName is None
    assert ap.outputPath is None
    assert ap.resFile is None


def test_create_alya_particles_with_paths():
    """
    Test initialization of Alya particles object with existing paths.
    """
    #
    # Cilean and create directory
    #
    casePath = os.path.join(tmp_test_path, "create_alya_particles_with_paths")
    shutil.rmtree(casePath, ignore_errors=True)
    if not os.path.exists(casePath):
        os.makedirs(casePath)

    #
    # Create dummy files
    #
    caseName = "case"
    with open(os.path.join(casePath, "{}.dat".format(caseName)), "w") as f:
        f.write("foo")
    with open(os.path.join(casePath, "{}.pts.res".format(caseName)), "w") as f:
        f.write("bar")

    ap = AlyaParticles(
        casePath=casePath,
        caseName=caseName,
        outputPath=os.path.join(casePath, "ptsOutput"),
    )

    assert ap.casePath == casePath
    assert ap.caseName == caseName
    assert "ptsOutput" in ap.outputPath
    assert os.path.isdir(ap.outputPath)
    assert not ap.resFile is None
    assert os.path.isfile(ap.resFile)

    #
    # Try bad case name
    #
    ap.setCaseName("zap", verbose=True)
    assert not os.path.isfile(ap.resFile)

    #
    # Try existing output dir
    #
    ap.setOutputPath(casePath)
    assert os.path.isdir(casePath)
    assert os.path.isfile(os.path.join(casePath, "{}.dat".format(caseName)))


def test_alya_particles_processing():
    """
    Test processing Alya particle output file.
    """
    #
    # Cilean and create directory
    #
    casePath = os.path.join(tmp_test_path, "alya_particles_processing")
    shutil.rmtree(casePath, ignore_errors=True)
    if not os.path.exists(casePath):
        os.makedirs(casePath)

    caseName = "case_multi_drop"
    #
    # Copy results file
    #
    shutil.copy(
        os.path.join(test_path, "data", "{}.pts.res".format(caseName)),
        os.path.join(casePath, "{}.pts.res".format(caseName)),
    )

    #
    # Create dummy files
    #
    with open(os.path.join(casePath, "{}.dat".format(caseName)), "w") as f:
        f.write("foo")

    ap = AlyaParticles(
        casePath=casePath,
        caseName=caseName,
        outputPath=os.path.join(casePath, "ptsOutput"),
    )

    #
    # Test reading the header
    #
    ap.readResHeader()
    assert ap.resHeader[0] == "T"

    #
    # Test processing the resut file
    #
    ap.processResFile(
        writeToVTK=True, buffer_size=5, storeData=True, key_list=["DIAMK", "TEMPK"]
    )
    assert os.path.isfile(os.path.join(ap.outputPath, "{}_0000.vtu".format(caseName)))
    assert os.path.isfile(os.path.join(ap.outputPath, "{}_0003.vtu".format(caseName)))
    assert os.path.isfile(os.path.join(ap.outputPath, "{}.pvd".format(caseName)))
    assert len(list(ap.rawdata["DIAMK"].keys())) == 3
    assert len(ap.rawdata["DIAMK"][0]) == 25
    assert len(ap.rawdata["DIAMK"][1]) == 25
    assert len(ap.rawdata["DIAMK"][2]) == 25
    assert len(ap.rawdata["TEMPK"][2]) == 25
    assert np.abs(np.max(ap.rawdata["DIAMK"][2]) - 0.000100142985) < 1e-9
    assert np.abs(np.min(ap.rawdata["DIAMK"][2]) - 0.000100122474) < 1e-9
    assert np.abs(np.max(ap.rawdata["TEMPK"][2]) - 303.7595) < 1e-3
    assert np.abs(np.min(ap.rawdata["TEMPK"][2]) - 303.0493) < 1e-3


def test_alya_particles_processing_with_time_step_match():
    """
    Test processing Alya particle output file with limiting the time steps.
    """
    #
    # Cilean and create directory
    #
    casePath = os.path.join(tmp_test_path, "alya_particles_reading_with_match")
    shutil.rmtree(casePath, ignore_errors=True)
    if not os.path.exists(casePath):
        os.makedirs(casePath)

    caseName = "case_single_drop"
    #
    # Copy results file
    #
    shutil.copy(
        os.path.join(test_path, "data", "{}.pts.res".format(caseName)),
        os.path.join(casePath, "{}.pts.res".format(caseName)),
    )

    ap = AlyaParticles(
        casePath=casePath,
        caseName=caseName,
        outputPath=os.path.join(casePath, "ptsOutput"),
    )

    #
    # Test reading the header
    #
    ap.readResHeader()
    assert ap.resHeader[0] == "T"

    #
    # Test processing the resut file
    #
    ap.processResFile(
        data=dict(
            TimeLimiting_Method="matchPvdFile",
            TimeLimiting_PvdFileToMatchTimeSteps=os.path.join(
                test_path, "data", "matching.pvd"
            ),
        ),
        writeToVTK=True,
    )
    assert os.path.isfile(os.path.join(ap.outputPath, "{}_0000.vtu".format(caseName)))
    assert not os.path.isfile(
        os.path.join(ap.outputPath, "{}_0044.vtu".format(caseName))
    )
    assert os.path.isfile(os.path.join(ap.outputPath, "{}.pvd".format(caseName)))

    base = BaseVTK()
    base.readPVD(os.path.join(ap.outputPath, "{}.pvd".format(caseName)))
    assert np.abs(base.times[0] - 0.0308) < 1e-8


def test_alya_particles_processing_with_manual_time_step_match():
    """
    Test processing Alya particle output file with limiting the time steps.
    """
    #
    # Cilean and create directory
    #
    casePath = os.path.join(tmp_test_path, "alya_particles_reading_with_manual_match")
    shutil.rmtree(casePath, ignore_errors=True)
    if not os.path.exists(casePath):
        os.makedirs(casePath)

    caseName = "case_single_drop"
    #
    # Copy results file
    #
    shutil.copy(
        os.path.join(test_path, "data", "{}.pts.res".format(caseName)),
        os.path.join(casePath, "{}.pts.res".format(caseName)),
    )

    ap = AlyaParticles(
        casePath=casePath,
        caseName=caseName,
        outputPath=os.path.join(casePath, "ptsOutput"),
    )

    #
    # Test reading the header
    #
    ap.readResHeader()
    assert ap.resHeader[0] == "T"

    #
    # Test processing the resut file
    #
    ap.processResFile(
        data=dict(
            TimeLimiting_Method="manual",
            TimeLimiting_UserSpecifiedTimeStepMethod=[0.0],
            ExistenceLevelsToExtract=[-5, -1],
        ),
        writeToVTK=True,
    )
    assert os.path.isfile(os.path.join(ap.outputPath, "{}_0000.vtu".format(caseName)))
    assert os.path.isfile(os.path.join(ap.outputPath, "{}.pvd".format(caseName)))

    base = BaseVTK()
    base.readPVD(os.path.join(ap.outputPath, "{}.pvd".format(caseName)))
    assert np.abs(base.times[0] - 0.0) < 1e-8
