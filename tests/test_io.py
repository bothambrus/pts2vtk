#################################################################################
#   pts2vtk                                                                     #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import numpy as np

from .context import pts2vtk
from pts2vtk.io import array_to_string


def test_array_to_string():
    """
    Test converting 1D and 2D numpy arrays to string.
    """
    a = np.array([1.0, 2.0, 3.0, 4.0, 5.0])
    st, npoin, ndime = array_to_string(a)
    assert npoin == 5
    assert ndime == 1
    assert st == "1.0 2.0 3.0 4.0 5.0 "

    b = np.array(
        [
            [1.0, 1.1, 1.2],
            [2.0, 2.1, 2.2],
            [3.0, 3.1, 3.2],
            [4.0, 4.1, 4.2],
            [5.0, 5.1, 5.2],
        ]
    )
    st, npoin, ndime = array_to_string(b)
    assert npoin == 5
    assert ndime == 3
    assert st == "1.0 1.1 1.2 2.0 2.1 2.2 3.0 3.1 3.2 4.0 4.1 4.2 5.0 5.1 5.2 "
