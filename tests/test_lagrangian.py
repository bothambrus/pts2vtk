#################################################################################
#   pts2vtk                                                                     #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import shutil
import numpy as np
import xml.etree.ElementTree as ET

from .context import pts2vtk, getTestTmpPath
from pts2vtk.lagrangian import LagrangianVTK

tmp_test_path = getTestTmpPath()


def test_create_lagrangian():
    """
    Test initialization of Lagrangian VTK object.
    """
    lagrangian = LagrangianVTK()
    assert len(lagrangian.times) == 0
    assert len(lagrangian.fileNames) == 0


def test_writing_1D_VTU_file():
    """
    Test writing a VTU file from this class with 1D location data.
    """
    #
    # Create lagrangian
    #
    lagrangian = LagrangianVTK()

    #
    # Cilean and create directory
    #
    case_path = os.path.join(tmp_test_path, "lagrangian_writing_1D_VTU_file")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    xyz = np.zeros([5])
    xyz[0] = 1.1
    xyz[1] = 2.1
    xyz[2] = 3.1
    xyz[3] = 4.1
    xyz[4] = 5.1

    #
    # Write VTU
    #
    fn = os.path.join(case_path, "test.vtu")
    lagrangian.writeVTU(fn, xyz)

    assert lagrangian.fileNames[0] == fn
    assert lagrangian.times[0] == 1

    #
    # Read and check resulting file
    #
    tree = ET.parse(fn)
    root = tree.getroot()

    assert root.attrib["type"] == "UnstructuredGrid"
    assert root.attrib["version"] == "0.1"
    assert root.attrib["byte_order"] == "LittleEndian"
    assert root[0][0][0][0].attrib["NumberOfComponents"] == "1"
    assert root[0][0][0][0].attrib["format"] == "ascii"
    assert root[0][0][0][0].attrib["type"] == "Float32"
    assert root[0][0][0][0].text == "1.1 2.1 3.1 4.1 5.1 "
    assert root[0][0][1][0].attrib["Name"] == "connectivity"
    assert root[0][0][2][0].attrib["Name"] == "Points"
    assert root[0][0][2][0].attrib["NumberOfComponents"] == "1"
    assert root[0][0][2][0].attrib["format"] == "ascii"
    assert root[0][0][2][0].attrib["type"] == "Float32"
    assert root[0][0][2][0].text == "1.1 2.1 3.1 4.1 5.1 "


def test_writing_VTU_file():
    """
    Test writing a VTU file from this class.
    """
    #
    # Create lagrangian
    #
    lagrangian = LagrangianVTK()

    #
    # Cilean and create directory
    #
    case_path = os.path.join(tmp_test_path, "lagrangian_writing_VTU_file")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    xyz = np.zeros([5, 3])
    xyz[0, :] = [1.1, 1.2, 1.3]
    xyz[1, :] = [2.1, 2.2, 2.3]
    xyz[2, :] = [3.1, 3.2, 3.3]
    xyz[3, :] = [4.1, 4.2, 4.3]
    xyz[4, :] = [5.1, 5.2, 5.3]

    #
    # Write VTU
    #
    fn = os.path.join(case_path, "test.vtu")
    lagrangian.writeVTU(fn, xyz)

    assert lagrangian.fileNames[0] == fn
    assert lagrangian.times[0] == 1

    #
    # Read and check resulting file
    #
    tree = ET.parse(fn)
    root = tree.getroot()

    assert root.attrib["type"] == "UnstructuredGrid"
    assert root.attrib["version"] == "0.1"
    assert root.attrib["byte_order"] == "LittleEndian"
    assert root[0][0][0][0].attrib["NumberOfComponents"] == "3"
    assert root[0][0][0][0].attrib["format"] == "ascii"
    assert root[0][0][0][0].attrib["type"] == "Float32"
    assert (
        root[0][0][0][0].text
        == "1.1 1.2 1.3 2.1 2.2 2.3 3.1 3.2 3.3 4.1 4.2 4.3 5.1 5.2 5.3 "
    )
    assert root[0][0][1][0].attrib["Name"] == "connectivity"
    assert root[0][0][2][0].attrib["Name"] == "Points"
    assert root[0][0][2][0].attrib["NumberOfComponents"] == "3"
    assert root[0][0][2][0].attrib["format"] == "ascii"
    assert root[0][0][2][0].attrib["type"] == "Float32"
    assert (
        root[0][0][2][0].text
        == "1.1 1.2 1.3 2.1 2.2 2.3 3.1 3.2 3.3 4.1 4.2 4.3 5.1 5.2 5.3 "
    )


def test_writing_VTU_file_given_time():
    """
    Test writing a VTU file from this class with giving the time of the step.
    """
    #
    # Create lagrangian
    #
    lagrangian = LagrangianVTK()

    #
    # Cilean and create directory
    #
    case_path = os.path.join(tmp_test_path, "lagrangian_writing_VTU_file_time")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    xyz = np.zeros([5, 3])
    xyz[0, :] = [1.1, 1.2, 1.3]
    xyz[1, :] = [2.1, 2.2, 2.3]
    xyz[2, :] = [3.1, 3.2, 3.3]
    xyz[3, :] = [4.1, 4.2, 4.3]
    xyz[4, :] = [5.1, 5.2, 5.3]

    #
    # Write VTU
    #
    fn = os.path.join(case_path, "test.vtu")
    lagrangian.writeVTU(fn, xyz, t=0.1)

    assert lagrangian.fileNames[0] == fn
    assert lagrangian.times[0] == 0.1


def test_writing_VTU_file_given_time_by_data():
    """
    Test writing a VTU file from this class with giving the time of the step.
    """
    #
    # Create lagrangian
    #
    lagrangian = LagrangianVTK()

    #
    # Cilean and create directory
    #
    case_path = os.path.join(tmp_test_path, "lagrangian_writing_VTU_file_time_by_data")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    xyz = np.zeros([5, 3])
    xyz[0, :] = [1.1, 1.2, 1.3]
    xyz[1, :] = [2.1, 2.2, 2.3]
    xyz[2, :] = [3.1, 3.2, 3.3]
    xyz[3, :] = [4.1, 4.2, 4.3]
    xyz[4, :] = [5.1, 5.2, 5.3]

    fn = os.path.join(case_path, "test.vtu")

    #
    # Write VTU
    #
    data = {}
    data["t"] = np.ones([5]) * 0.2
    lagrangian.writeVTU(fn, xyz, data=data)
    assert lagrangian.times[0] == 0.2

    data = {}
    data["T"] = np.ones([5]) * 0.3
    lagrangian.writeVTU(fn, xyz, data=data)
    assert lagrangian.times[1] == 0.3


def test_writing_VTU_file_with_data():
    """
    Test writing a VTU file from this class.
    """
    #
    # Create lagrangian
    #
    lagrangian = LagrangianVTK()

    #
    # Cilean and create directory
    #
    case_path = os.path.join(tmp_test_path, "lagrangian_writing_VTU_file_with_data")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    xyz = np.zeros([5, 3])
    xyz[0, :] = [1.1, 1.2, 1.3]
    xyz[1, :] = [2.1, 2.2, 2.3]
    xyz[2, :] = [3.1, 3.2, 3.3]
    xyz[3, :] = [4.1, 4.2, 4.3]
    xyz[4, :] = [5.1, 5.2, 5.3]

    data = {}
    data["T"] = np.ones([5]) * 0.2
    data["DIAMK"] = np.zeros([5])
    data["DIAMK"][0] = 6e-6
    data["DIAMK"][1] = 7e-6
    data["DIAMK"][2] = 8e-6
    data["DIAMK"][3] = 9e-6
    data["DIAMK"][4] = 1e-5

    data["VELOC"] = np.zeros([5, 3])
    data["VELOC"][0] = [1.6, 1.7, 1.8]
    data["VELOC"][1] = [2.6, 2.7, 2.8]
    data["VELOC"][2] = [3.6, 3.7, 3.8]
    data["VELOC"][3] = [4.6, 4.7, 4.8]
    data["VELOC"][4] = [5.6, 5.7, 5.8]

    #
    # Write VTU
    #
    fn = os.path.join(case_path, "test.vtu")
    lagrangian.writeVTU(fn, xyz, data=data)

    assert lagrangian.fileNames[0] == fn
    assert lagrangian.times[0] == 0.2

    #
    # Read and check resulting file
    #
    tree = ET.parse(fn)
    root = tree.getroot()

    assert root.attrib["type"] == "UnstructuredGrid"
    assert root.attrib["version"] == "0.1"
    assert root.attrib["byte_order"] == "LittleEndian"
    assert root[0][0][0][0].attrib["NumberOfComponents"] == "3"
    assert root[0][0][0][0].attrib["format"] == "ascii"
    assert root[0][0][0][0].attrib["type"] == "Float32"
    assert (
        root[0][0][0][0].text
        == "1.1 1.2 1.3 2.1 2.2 2.3 3.1 3.2 3.3 4.1 4.2 4.3 5.1 5.2 5.3 "
    )
    assert root[0][0][1][0].attrib["Name"] == "connectivity"

    assert root[0][0][2][0].attrib["Name"] == "Points"
    assert root[0][0][2][0].attrib["NumberOfComponents"] == "3"
    assert root[0][0][2][0].attrib["format"] == "ascii"
    assert root[0][0][2][0].attrib["type"] == "Float32"
    assert (
        root[0][0][2][0].text
        == "1.1 1.2 1.3 2.1 2.2 2.3 3.1 3.2 3.3 4.1 4.2 4.3 5.1 5.2 5.3 "
    )

    assert root[0][0][2][1].attrib["Name"] == "T"
    assert root[0][0][2][1].attrib["NumberOfComponents"] == "1"
    assert root[0][0][2][1].attrib["format"] == "ascii"
    assert root[0][0][2][1].attrib["type"] == "Float32"
    assert root[0][0][2][1].text == "0.2 0.2 0.2 0.2 0.2 "

    assert root[0][0][2][2].attrib["Name"] == "DIAMK"
    assert root[0][0][2][2].attrib["NumberOfComponents"] == "1"
    assert root[0][0][2][2].attrib["format"] == "ascii"
    assert root[0][0][2][2].attrib["type"] == "Float32"
    assert root[0][0][2][2].text == "6e-06 7e-06 8e-06 9e-06 1e-05 "

    assert root[0][0][2][3].attrib["Name"] == "VELOC"
    assert root[0][0][2][3].attrib["NumberOfComponents"] == "3"
    assert root[0][0][2][3].attrib["format"] == "ascii"
    assert root[0][0][2][3].attrib["type"] == "Float32"
    assert (
        root[0][0][2][3].text
        == "1.6 1.7 1.8 2.6 2.7 2.8 3.6 3.7 3.8 4.6 4.7 4.8 5.6 5.7 5.8 "
    )
