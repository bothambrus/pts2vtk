#################################################################################
#   pts2vtk                                                                     #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy
import os
import sys

import numpy as np

from pts2vtk.base import BaseVTK
from pts2vtk.lagrangian import LagrangianVTK

import pts2vtk.context  # isort:skip
from lectio.grid import selectGridMethod  # isort:skip


class AlyaParticles:
    """
    Class to hold and process the results of the partis module of Alya.

    Parameters
    ----------
    casePath : str
        Path to the Alya case.
    caseName : str
        Name of the Alya case.
    outputPath : str
        Path where the output of data processing is written.
    """

    def __init__(self, casePath=None, caseName=None, outputPath=None):
        #
        # Intitialize data
        #
        self.casePath = None
        self.caseName = None
        self.outputPath = None
        self.resFile = None
        self.resHeader = None
        self.resNvar = None
        self.resExistCol = None
        self.resTimeCol = None
        #
        # List of excluded variables, time will we saved another way.
        #
        self.resExcludeVar = ["T", "EXIST"]
        self.resIncludeVar = None
        self.resIncludeVarIndex = None

        #
        # Initialize locally stored data
        #
        self.rawdata = {}

        #
        # Process input data
        #
        self.setCasePath(casePath)
        self.setCaseName(caseName, verbose=True)
        self.setOutputPath(outputPath)

    def setCasePath(self, casePath):
        """
        Set case path.
        """
        if (not casePath is None) and os.path.isdir(casePath):
            self.casePath = casePath
        else:
            print("Given case path: {} does not exist.".format(casePath))
            sys.stdout.flush()

        self.setCaseName()

    def setCaseName(self, caseName=None, verbose=False):
        """
        Set case name.
        """
        #
        # Rewrite case name unless it is None
        #
        if not caseName is None:
            self.caseName = caseName

        #
        # Try finding files knowing the case name
        #
        if not self.casePath is None:
            #
            # Try determining if it is an Alya case
            #
            datFile = os.path.join(self.casePath, "{}.dat".format(self.caseName))
            if (not os.path.isfile(datFile)) and verbose:
                print(
                    "Seems like the Alya case does not exist. \n{} is not found!".format(
                        datFile
                    )
                )
                sys.stdout.flush()

            #
            # Try if the partis Result file exists
            #
            self.resFile = os.path.join(
                self.casePath, "{}.pts.res".format(self.caseName)
            )
            if (not os.path.isfile(self.resFile)) and verbose:
                print(
                    "Seems like the Partis output does not exist. \n{} is not found!".format(
                        self.resFile
                    )
                )
                sys.stdout.flush()

    def setOutputPath(self, outputPath):
        """
        Set output case path.
        """
        self.outputPath = outputPath

        if (not self.outputPath is None) and os.path.isdir(self.outputPath):
            print(
                "Output path seems to exists already: {}\nExisting files may be overwritten!".format(
                    self.outputPath
                )
            )
            sys.stdout.flush()
        else:
            #
            # Create path if does not exist
            #
            try:
                print("Creating output path.")
                sys.stdout.flush()
                os.makedirs(self.outputPath)
            except:
                print("Could not create output path: {}".format(self.outputPath))
                sys.stdout.flush()

    def readResHeader(self):
        """
        Read header of results file.
        """
        if not self.resFile is None:
            with open(self.resFile, "r") as f:
                line = True
                saveline = ""
                notFound = True
                self.resHeader = None
                #
                # Look for the first instance of switching from commented lines to uncommented lines
                #
                while line and notFound:
                    line = f.readline()
                    if line[0] != "#":
                        #
                        # If the switch happened, the previous line contained the full header
                        #
                        self.resHeader = saveline.strip("#").split()
                        notFound = False
                        break
                    saveline = line

                #
                # Print
                #
                if not self.resHeader is None:
                    print("Found header of Partis output file:")
                    for ik, k in enumerate(self.resHeader):
                        print("Column #{}: {}".format(ik + 1, k))
                    sys.stdout.flush()

        #
        # Process header
        #
        if not self.resHeader is None:
            #
            # Number of variables
            #
            self.resNvar = len(self.resHeader)

            #
            # Subtract excluded variables
            #
            self.resIncludeVar = []
            self.resIncludeVarIndex = {}
            for ik, k in enumerate(self.resHeader):
                if k in self.resExcludeVar:
                    self.resNvar -= 1
                else:
                    self.resIncludeVar.append(k)
                    self.resIncludeVarIndex[k] = ik

            #
            # Column of "EXIST" status of particle
            #
            self.resExistCol = self.resHeader.index("EXIST")
            self.resTimeCol = self.resHeader.index("T")

    def processResFile(
        self, data={}, storeData=False, writeToVTK=False, buffer_size=10000, key_list=[]
    ):
        """
        Go through Alya particle results file and process it.
        """
        #
        # Time limitations
        #
        doTimeMatching = False

        if "TimeLimiting_Method" in data.keys():
            if data["TimeLimiting_Method"] == "matchPvdFile":
                #
                # Get acceptable time steps
                #
                doTimeMatching = True
                pvd_to_match = BaseVTK()
                pvd_to_match.readPVD(data["TimeLimiting_PvdFileToMatchTimeSteps"])
                print("Attempting to match PVD time steps:")
                message = ""
                times_to_match = []
                for t in pvd_to_match.times:
                    message += "{:8.6g} ".format(t)
                    times_to_match.append(t)
                print(message)
                sys.stdout.flush()

            elif data["TimeLimiting_Method"] == "manual":
                #
                # Get acceptable time steps
                #
                doTimeMatching = True
                print("Attempting to match user defined time steps:")
                message = ""
                times_to_match = []

                for t in selectGridMethod(
                    data["TimeLimiting_UserSpecifiedTimeStepMethod"]
                ):
                    message += "{:8.6g} ".format(t)
                    times_to_match.append(t)
                print(message)
                sys.stdout.flush()

        #
        # Exitence levels to extract
        #
        if "ExistenceLevelsToExtract" in data.keys():
            ExistenceLevelsToExtract = list(data["ExistenceLevelsToExtract"])
        else:
            ExistenceLevelsToExtract = [-1]

        #
        # Re-read header just in case
        #
        self.readResHeader()

        if not self.resFile is None:
            with open(self.resFile, "r") as f:
                line = True

                #
                # Create buffer
                #
                if writeToVTK:
                    lagVTK = LagrangianVTK()
                buffer = np.empty([buffer_size, self.resNvar], dtype=np.float32)
                ilagr_loc = 0
                timePrev = -np.inf
                istep = -1

                #
                # Skip header and empty lines.
                #
                while line:
                    line = f.readline()
                    if len(line) == 0:
                        continue
                    if line[0] == "#":
                        continue

                    #
                    # Process line
                    #
                    data = line.split()
                    exist = int(data[self.resExistCol]) in ExistenceLevelsToExtract

                    #
                    # For now only process existing particles
                    #
                    doProcess = exist

                    #
                    # Match with other pvd file
                    #
                    if doTimeMatching:
                        time = float(data[self.resTimeCol])

                        doesMatch = False
                        matchingTime = None
                        for t in times_to_match:
                            if np.abs(time - t) < 1e-6:
                                doesMatch = True
                                matchingTime = t

                        if not doesMatch:
                            doProcess = False

                    if doProcess:
                        #
                        # Evaluate time
                        #
                        if doTimeMatching:
                            time = matchingTime
                        else:
                            time = float(data[self.resTimeCol])

                        #
                        # Check if time has jumped
                        #
                        if np.abs(time - timePrev) > 1e-8 and istep > -1:
                            #
                            # If time jumped process data of this time step, and reset counter
                            #
                            if writeToVTK:
                                self.writeCollectedDataToVTK(
                                    buffer, ilagr_loc, istep, timePrev, lagVTK
                                )

                            if storeData:
                                self.storeCollectedData(
                                    buffer, ilagr_loc, istep, timePrev, key_list
                                )

                        if np.abs(time - timePrev) > 1e-8:
                            #
                            # Reset variables
                            #
                            istep += 1
                            ilagr_loc = 0

                        #
                        # Expand buffer if necessary
                        #
                        if ilagr_loc >= buffer_size:
                            buffer_size += 10000
                            buffer = np.resize(buffer, (buffer_size, self.resNvar))

                        #
                        # Save to buffer
                        #
                        for ik, k in enumerate(self.resIncludeVar):
                            buffer[ilagr_loc, ik] = float(
                                data[self.resIncludeVarIndex[k]]
                            )

                        #
                        # Increment counter
                        #
                        ilagr_loc += 1
                        timePrev = time

                if writeToVTK:
                    #
                    # Write last time step
                    #
                    self.writeCollectedDataToVTK(
                        buffer, ilagr_loc, istep, timePrev, lagVTK
                    )

                    #
                    # Write collection
                    #
                    fn = os.path.join(self.outputPath, "{}.pvd".format(self.caseName))
                    lagVTK.writePVD(fn)

    def writeCollectedDataToVTK(self, buffer, ilagr_loc, istep, time, lagVTK):
        """
        Helper function writing collected data.
        """
        #
        # Print
        #
        print("Writing time step {} with t={} s to VTK file".format(istep, time))
        sys.stdout.flush()

        #
        # Get coordinates
        #
        x = buffer[0:ilagr_loc, self.resIncludeVar.index("COORX")]
        y = buffer[0:ilagr_loc, self.resIncludeVar.index("COORY")]
        z = buffer[0:ilagr_loc, self.resIncludeVar.index("COORZ")]
        xyz = x.reshape(len(x), 1)
        xyz = np.append(xyz, y.reshape(len(y), 1), axis=1)
        xyz = np.append(xyz, z.reshape(len(z), 1), axis=1)

        #
        # Get data
        #
        dataToVTK = {}
        for ik, k in enumerate(self.resIncludeVar):
            if not k in ["COORX", "COORY", "COORZ"]:
                dataToVTK[k] = buffer[0:ilagr_loc, ik]

        if (
            ("VELOX" in self.resIncludeVar)
            and ("VELOY" in self.resIncludeVar)
            and ("VELOZ" in self.resIncludeVar)
        ):
            u = buffer[0:ilagr_loc, self.resIncludeVar.index("VELOX")]
            v = buffer[0:ilagr_loc, self.resIncludeVar.index("VELOY")]
            w = buffer[0:ilagr_loc, self.resIncludeVar.index("VELOZ")]
            velo = u.reshape(len(u), 1)
            velo = np.append(velo, v.reshape(len(y), 1), axis=1)
            velo = np.append(velo, w.reshape(len(z), 1), axis=1)
            dataToVTK["VELOC"] = velo

        #
        # Write data
        #
        fn = os.path.join(self.outputPath, "{}_{:04d}.vtu".format(self.caseName, istep))
        lagVTK.writeVTU(fn, xyz, data=dataToVTK, t=time)

    def storeCollectedData(self, buffer, ilagr_loc, istep, time, key_list):
        """
        Helper function storing collected data.
        """
        #
        # Print
        #
        print("Storing time step {} with t={} s".format(istep, time))
        sys.stdout.flush()

        for key in key_list:
            if key in self.resIncludeVar:
                #
                # Initialize if not present
                #
                if not key in self.rawdata:
                    self.rawdata[key] = {}

                #
                # Save
                #
                self.rawdata[key][istep] = copy.deepcopy(
                    buffer[0:ilagr_loc, self.resIncludeVar.index(key)]
                )
