#################################################################################
#   pts2vtk                                                                     #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import xml.dom.minidom
import xml.etree.ElementTree as ET


class BaseVTK:
    """
    Base class to hold data of VTK case.
    """

    def __init__(self):
        self.fileNames = []
        self.times = []

    def writePVD(self, fileFullPath):
        """
        Write file that joins the time steps.

        Parameters
        ----------
        fileFullPath : str
            Path where joint VTK case file is to be written.
            e.g.: "./my_vtk/case.pvd"
        """
        with open(fileFullPath, "w") as outFile:
            #
            # Create PVD object
            #
            pvd = xml.dom.minidom.Document()
            pvd_root = pvd.createElementNS("VTK", "VTKFile")
            pvd_root.setAttribute("type", "Collection")
            pvd_root.setAttribute("version", "0.1")
            pvd_root.setAttribute("byte_order", "LittleEndian")
            pvd.appendChild(pvd_root)

            #
            # Create collection of time steps
            #
            collection = pvd.createElementNS("VTK", "Collection")
            pvd_root.appendChild(collection)

            #
            # Connect joint file to time steps
            #
            for i, fn in enumerate(self.fileNames):
                dataSet = pvd.createElementNS("VTK", "DataSet")
                dataSet.setAttribute("timestep", str(self.times[i]))
                dataSet.setAttribute("group", "")
                dataSet.setAttribute("part", "0")
                dataSet.setAttribute(
                    "file",
                    os.path.relpath(self.fileNames[i], os.path.dirname(fileFullPath)),
                )
                collection.appendChild(dataSet)

            #
            # Write joint VTK file
            #
            pvd.writexml(outFile, newl="\n")
            outFile.close()

    def readPVD(self, fileFullPath):
        """
        Read file that joins the time steps of a VTK case.

        Parameters
        ----------
        fileFullPath : str
            Path of the joint VTK case file.
            e.g.: "./my_vtk/case.pvd"
        """
        #
        # Read PVD file
        #
        tree = ET.parse(fileFullPath)
        root = tree.getroot()

        #
        # Initialize lists
        #
        self.fileNames = []
        self.times = []

        #
        # Get all names
        #
        if len(root) > 0:
            for i in range(len(root[0])):
                self.fileNames.append(root[0][i].attrib["file"])
                self.times.append(float(root[0][i].attrib["timestep"]))
