#################################################################################
#   pts2vtk                                                                     #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import xml.dom.minidom

import numpy as np

from pts2vtk.base import BaseVTK
from pts2vtk.io import array_to_string


class LagrangianVTK(BaseVTK):
    """
    Class aggregating instantaneous states of Lagrangian particle clouds into a temporal database.
    """

    def __init__(self):
        BaseVTK.__init__(self)

    def writeVTU(self, fileFullPath, xyz, data={}, t=None):
        """
        Parameters
        ----------
        fileFullPath : str
            Path where VTK time instance file is to be written.
            e.g.: "./my_vtk/step1.pvd"
        xyz: np.ndarray
            Coordinates of Lagrangian paticles with shape (npoin, ndime).
            The first index determines the order of the particle,
            and the second index the coorinate in space.
        data: dict
            Dictionary of further numpy arrays of additional data.
        """
        #
        # Process Dimensons
        #
        if len(xyz.shape) > 1:
            ndime = xyz.shape[1]
        else:
            ndime = 1

        #
        # Determine data size
        #
        npoin = xyz.shape[0]

        #
        # XML document and root element
        #
        doc = xml.dom.minidom.Document()
        root_element = doc.createElementNS("VTK", "VTKFile")
        root_element.setAttribute("type", "UnstructuredGrid")
        root_element.setAttribute("version", "0.1")
        root_element.setAttribute("byte_order", "LittleEndian")
        doc.appendChild(root_element)

        #
        # Add unstructured grid element to root
        #
        unstructuredGrid = doc.createElementNS("VTK", "UnstructuredGrid")
        root_element.appendChild(unstructuredGrid)

        #
        # Add one piece to unstructured grid
        #
        piece = doc.createElementNS("VTK", "Piece")
        piece.setAttribute("NumberOfPoints", "{}".format(npoin))
        piece.setAttribute("NumberOfCells", "0")
        unstructuredGrid.appendChild(piece)

        ##########
        # Points #
        ##########
        points = doc.createElementNS("VTK", "Points")
        piece.appendChild(points)

        #
        # Point location data
        #
        point_coords = doc.createElementNS("VTK", "DataArray")
        point_coords.setAttribute("type", "Float32")
        point_coords.setAttribute("format", "ascii")
        point_coords.setAttribute("NumberOfComponents", "{}".format(ndime))
        points.appendChild(point_coords)

        #
        # Flatten coordianate array
        #
        string, npoin_out, ndime_out = array_to_string(xyz)
        point_coords_data = doc.createTextNode(string)
        point_coords.appendChild(point_coords_data)

        #########
        # Cells #
        #########
        cells = doc.createElementNS("VTK", "Cells")
        piece.appendChild(cells)

        #
        # Cell locations
        #
        cell_connectivity = doc.createElementNS("VTK", "DataArray")
        cell_connectivity.setAttribute("type", "Int32")
        cell_connectivity.setAttribute("Name", "connectivity")
        cell_connectivity.setAttribute("format", "ascii")
        cells.appendChild(cell_connectivity)

        #
        # Cell location data
        #
        connectivity = doc.createTextNode("0")
        cell_connectivity.appendChild(connectivity)

        cell_offsets = doc.createElementNS("VTK", "DataArray")
        cell_offsets.setAttribute("type", "Int32")
        cell_offsets.setAttribute("Name", "offsets")
        cell_offsets.setAttribute("format", "ascii")
        cells.appendChild(cell_offsets)
        offsets = doc.createTextNode("0")
        cell_offsets.appendChild(offsets)

        cell_types = doc.createElementNS("VTK", "DataArray")
        cell_types.setAttribute("type", "UInt8")
        cell_types.setAttribute("Name", "types")
        cell_types.setAttribute("format", "ascii")
        cells.appendChild(cell_types)
        types = doc.createTextNode("1")
        cell_types.appendChild(types)

        ##################
        # Data at Points #
        ##################
        point_data = doc.createElementNS("VTK", "PointData")
        piece.appendChild(point_data)

        #
        # Add the location itself as data of the points
        #
        point_coords_2 = doc.createElementNS("VTK", "DataArray")
        point_coords_2.setAttribute("Name", "Points")
        point_coords_2.setAttribute("NumberOfComponents", str(ndime))
        point_coords_2.setAttribute("type", "Float32")
        point_coords_2.setAttribute("format", "ascii")
        point_data.appendChild(point_coords_2)

        point_coords_2_Data = doc.createTextNode(string)
        point_coords_2.appendChild(point_coords_2_Data)

        #
        # Add data
        #
        for k in data.keys():
            d = data[k]
            string, npoin_out, ndime_out = array_to_string(d)

            toAppend = doc.createElementNS("VTK", "DataArray")
            toAppend.setAttribute("Name", k)
            toAppend.setAttribute("NumberOfComponents", str(ndime_out))
            toAppend.setAttribute("type", "Float32")
            toAppend.setAttribute("format", "ascii")
            point_data.appendChild(toAppend)
            toAppendData = doc.createTextNode(string)
            toAppend.appendChild(toAppendData)

        #
        # Dummy cell data (cells make no sense for Lagrangian particle clouds)
        #
        dummy_cell_data = doc.createElementNS("VTK", "CellData")
        piece.appendChild(dummy_cell_data)

        #
        # Write to file and exit
        #
        with open(fileFullPath, "w") as outFile:
            doc.writexml(outFile, newl="\n")

        #
        # Register file name
        #
        self.fileNames.append(fileFullPath)

        #
        # Try determine time based on the input
        #
        if "t" in data.keys():
            t = np.min(data["t"])
        if "T" in data.keys():
            t = np.min(data["T"])

        #
        # Just give the order of this file as "time"
        #
        if t is None:
            t = len(self.fileNames)

        #
        # Register time
        #
        self.times.append(t)
