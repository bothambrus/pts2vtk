#################################################################################
#   pts2vtk                                                                     #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import json
import os


class ProcessingConfig:
    """
    Class to hold configuration data of Alya particle output processing.
    """

    def __init__(self, json_path=os.path.abspath("pts2vtkInputFile.json")):
        #
        # Initialize data
        #
        self.data = {}
        self.json_path = json_path

        #
        # Read json if exists
        #
        self.readConfig()

    def readConfig(self):
        """
        Read from path
        """
        if os.path.isfile(self.json_path):
            #
            # Read if exists
            #
            with open(self.json_path, "r") as f:
                self.data = json.load(f)

    def writeConfig(self):
        """
        Write to path
        """
        with open(self.json_path, "w") as f:
            json.dump(self.data, f, sort_keys=True, indent=4)
