#################################################################################
#   pts2vtk                                                                     #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np


def array_to_string(vect):
    """
    Convert 1D or 2D array to as string written to the VTK data files.
    """
    vect_loc = np.array(vect)

    #
    # Determine array size
    #
    shape = vect_loc.shape
    string = str()
    npoin = shape[0]
    if len(shape) > 1:
        ndime = shape[1]
    else:
        ndime = 1

    #
    # Create flattened string
    #
    if ndime == 1:
        for i in range(npoin):
            string += "{} ".format(vect_loc[i])
    else:
        for i in range(npoin):
            for j in range(ndime):
                string += "{} ".format(vect_loc[i, j])

    #
    # Return string and sizes
    #
    return string, npoin, ndime
